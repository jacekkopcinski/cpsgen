#include "sound.h"
#include <iostream>

int play2(std::valarray<std::complex<double>> &input,int rate)
{
sf::SoundBuffer buffer;
std::vector<sf::Int16> samples;
for (int i = 0; i < sizeof(input); i++)
    {
        samples.push_back(input[i].real());
        std::cout << samples[i];
        }
buffer.loadFromSamples(&samples[0], samples.size(), 2, rate);
sf::Sound sound;
sound.setBuffer(buffer);
sound.play();
return 0;
}
