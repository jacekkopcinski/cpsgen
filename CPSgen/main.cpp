// CPSgen.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include <vector>
#include <valarray>
#include <fstream>
#include <complex>
#include "FFT.h"
#include "audio.h"
#include "sound.h"
#include "algorytm.h"

void  writeToFile(std::string path, std::valarray<std::complex<double>>& input)
{
	std::ofstream outputFile(path.c_str());
unsigned int i = 0;
	for (; i <= input.size(); i++)
	{
		outputFile << input[i].real()<<" " << input[i].imag()<<"j" << std::endl;
	}

	outputFile.close();
}
void  writeToFile(std::string path, std::vector<std::complex<double>>& input)
{
	std::ofstream outputFile(path.c_str());
    unsigned int i = 0;

	for (; i < input.size(); i++)
	{
		outputFile << input[i].real()<<" " << input[i].imag()<<"j" << std::endl;
	}

	outputFile.close();
}
double sciToDub(const std::string& str)
{
	using namespace std;
	stringstream ss(str);
	double d = 0;
	ss >> d;

	if (ss.fail()) {
		string s = "Unable to format ";
		s += str;
		s += " as a number!";
		throw (s);
	}

	return (d);
}
int readGWM(std::string path, std::vector<double>& gwm_out, std::vector<double>& biny_out)
{
	using namespace std;
	string line;
	ifstream myfile(path);
	int count = 0;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			//istringstream;
			size_t przecinek = line.find(',');
			string smp = line.substr(0, przecinek);
			string tmp = line.substr(przecinek + 1);
			gwm_out.push_back(sciToDub(line.substr(0, przecinek)));
			biny_out.push_back(sciToDub(line.substr(przecinek + 1)));
			count++;
		}
		myfile.close();
		return count;
	}
	else cout << "Unable to open file";
	return -1;
}
int main(int argc, char **argv)
{
    //ARGUMENTY
    std::string path;
    int data_dump=0;
    if (argc>=2)
    {
    path=argv[1];
    if (argc>=3) data_dump=atoi(argv[2]);
    }
    else
    {
    std::cout<<"Invalid arguments. Check PSD file path." <<std::endl;
    }
    std::cout << "processing " << path << std::endl;
	std::vector<double> gwm, biny;
	readGWM(path, gwm, biny);

	std::vector<std::complex<double>> pre_ifft;
	double T = 1/(2*biny.back());
	double Fp = (2*biny.back());
	pre_ifft=process(gwm,biny,T);
	//kowersja vector->valarray
	std::valarray<std::complex<double>> pre_ifft2(pre_ifft.data(), pre_ifft.size());
	ifft(pre_ifft2);

	if (data_dump)
	{
	writeToFile(path+"_out.txt",pre_ifft2);
	writeToFile(path+"_pre_ifft.txt",pre_ifft);
	}
	std::cout << "playing (ctrl+c to stop)" <<std::endl;
	play(pre_ifft2,Fp);
}
