#include "algorytm.h"
#include <algorithm>
#include <math.h>
std::vector<std::complex<double>> process(std::vector<double>& gwm, std::vector<double>& biny, double T)
{
	std::vector<std::complex<double>> out;
	int N = 2 * (gwm.size() - 1);
	out.reserve(N);
	std::complex<double> tmp = std::sqrt((std::complex<double>)N / T * gwm[0]);
	out.push_back(tmp);
	int i = 0;
	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 2 * M_PI);
	for (i = 1; i < N / 2 - 1; i++)
	{
		double theta_m = distribution(generator);
		std::complex<double> real =  std::sqrt((std::complex<double>)N / T * gwm[i])*sin(theta_m);
		std::complex<double> imag = -std::sqrt((std::complex<double>)N / T * gwm[i])*cos(theta_m);
		tmp = real + imag;
		out.push_back(tmp);
	}
	i = N / 2 - 1;
	double theta_n2;
	//bernoulli distribution
	if (distribution(generator)>M_PI) theta_n2=M_PI/2; else theta_n2=3*M_PI/2;
	tmp = std::sqrt((std::complex<double>)N / T * gwm[i]) * sin(theta_n2);
	std::complex<double> testvar1=std::sqrt(N / T  * sin(theta_n2));
	std::complex<double> testvar2=N / T * gwm[i] * sin(theta_n2);
	out.push_back(tmp);
	for (i = i + 1; i <= N - 1; i++)
	{
        tmp=out[N - i - 1].real() - out[N - i - 1].imag();
		out.push_back(tmp);
	}
	return out;
}
