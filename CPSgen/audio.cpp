#include "audio.h"

int play(std::valarray<std::complex<double>> &input, int fs)
{
static char *device = "default";
int BUFFER_LEN=input.size();
int16_t buffer [BUFFER_LEN];
int err;

    snd_pcm_t *handle;
    snd_pcm_sframes_t frames;


    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
            printf("Playback open error: %s\n", snd_strerror(err));
            exit(EXIT_FAILURE);
    }

    //SND_PCM_FORMAT_FLOAT
    //SND_PCM_FORMAT_S16_LE
    if ((err = snd_pcm_set_params(handle,
                                  SND_PCM_FORMAT_S16_LE,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,
                                  1,
                                  fs,
                                  1,
                                  500000)) < 0) {
            printf("Playback open error2: %s\n", snd_strerror(err));
            exit(EXIT_FAILURE);
    }

//SIGNAL
//to sint
        for (int k=0; k<BUFFER_LEN; k++){
            double f = input[k].real();
            f = f * 32768;
            if( f > 32767 ) f = 32767;
            if( f < -32768 ) f = -32768;

            buffer[k] = (int16_t) f;
            }

        while(1){
        //buffer to sound driver
            frames = snd_pcm_writei(handle, buffer, BUFFER_LEN);
            }

    snd_pcm_close(handle);
    return 0;
}
